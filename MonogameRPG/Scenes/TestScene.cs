﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGEngine;
namespace MonogameRPG
{
    public class TestScene : Scene
    {

        public TestScene(string name) : base(name)
        {
            
        }

        protected override void OnSceneEnter()
        {
            Console.WriteLine("Entered Scene");
        }

        protected override void OnSceneExit()
        {
            Console.WriteLine("Exit Scene");
        }

        protected override void SetupEntityComposer()
        {
            this.entityComposer.RegisterEntityFactory("CameraFactory", new CameraFactory());
            this.entityComposer.RegisterEntityFactory("TileMapFactory", new TileMapFactory());

        }

        protected override void SetupScene()
        {
            this.RegisterEntity("MainCamera", this.entityComposer.ComposeEntity("CameraFactory"));
            this.Find("MainCamera").Position += new Microsoft.Xna.Framework.Vector2(512, 512);
            this.RegisterEntity("Tile Map", this.entityComposer.ComposeEntity("TileMapFactory"));
        }
    }
}
