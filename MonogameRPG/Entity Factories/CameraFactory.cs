﻿using RPGEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonogameRPG
{
    public class CameraFactory : IEntityFactory
    {
        public Entity MakeEntity()
        {
            Entity camera = new Entity("MainCamera");
            Camera camComponent = new Camera(camera);
            camComponent.Name = "Camera";
            camera.AddComponent(camComponent);
            
            return camera;
        }
    }
}
