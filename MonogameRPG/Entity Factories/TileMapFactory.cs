﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGEngine;
namespace MonogameRPG
{
    class TileMapFactory : IEntityFactory
    {
        public Entity MakeEntity()
        {
            Entity tileMap = new Entity("Tile Map");
            TileMapRenderer tileMapRenderer = new TileMapRenderer(tileMap, "Maps/Prototype Map", "Maps/");
            tileMapRenderer.Culling = true;
            tileMapRenderer.Name = "Tile Map Renderer";
            tileMap.AddComponent(tileMapRenderer);
            return tileMap;
        }
    }
}
