﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGEngine;
using Microsoft.Xna.Framework;

namespace MonogameRPG
{
    //Class for a component used to control an animated sprite.
    public class CharacterController : GameController
    {
        //Animated sprite renderer of the game character
        private AnimatedSpriteRenderer sprite;
        //float to store the speed of motion of the game character
        private float speed;
        //Vector2 to describe the motion of the game character.
        private Vector2 motion;
        //constructor of the character controller
        public CharacterController(Entity e, InputCommandSet commandSet,AnimatedSpriteRenderer sprite, float speed) :base(e,commandSet)
        {
            this.sprite = sprite;
            this.speed = speed;
        }
        //Override to handle input.
        protected override void HandleInput(GameTime gameTime)
        {
            //Reset the motion vector to zero.
            Vector2 motion = Vector2.Zero;
            //Adjust the motion of the based on the up, down, left, right commands in the command set.
            if (commands.IsCommandPressed("Up"))
                motion.Y--;
            
            if (commands.IsCommandPressed("Down"))       
                motion.Y++;
            
            if (commands.IsCommandPressed("Right"))            
                motion.X++;
            
            if (commands.IsCommandPressed("Left"))            
                motion.X--;
            
            //If the motion is not zero...
            if(motion != Vector2.Zero)
            {
                //Normalize the motion vector so diagonal movement does not double to movement speed.
                motion.Normalize();
                //Update the positon of the entity based on the motion vector, speed , and time elapased between now and the last frame.
                Entity.Position += motion * speed * (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                //Update the animation played based on the motion of the character.
                UpdateSpriteAnimation(motion);
                //Allow the animation to be played.
                sprite.IsAnimating = true;
            }
            //Else do not play an animation.
            else
            {
                sprite.IsAnimating = false;
            }            
                                    

        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }


        private void UpdateSpriteAnimation(Vector2 motion)
        {
            //Get the angle of motion of the sprite.
            float motionAngle = (float)Math.Atan2(motion.Y, motion.X);

            //Based on the angle of motion it chooses the proper animation.
            //NOTE: uncomment the motion reassignment in each if statement to restrict motion up down left right.
            if (motionAngle >= -MathHelper.PiOver4 &&
                motionAngle <= MathHelper.PiOver4)
            {
                sprite.CurrentAnimationName = "Right";
                //  motion = new Vector2(1f, 0f);
            }
            else if (motionAngle >= MathHelper.PiOver4 &&
                     motionAngle <= 3f * MathHelper.PiOver4)
            {
                sprite.CurrentAnimationName = "Down";
                // motion = new Vector2(0f, 1f);
            }
            else if (motionAngle <= -MathHelper.PiOver4 &&
                     motionAngle >= -3f * MathHelper.PiOver4)
            {
                sprite.CurrentAnimationName = "Up";
                // motion = new Vector2(0f, -1f);
            }
            else
            {
                sprite.CurrentAnimationName = "Left";
                //  motion = new Vector2(-1f, 0f);
            }
        }


    }
}
