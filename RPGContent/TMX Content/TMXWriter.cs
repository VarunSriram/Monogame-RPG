﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;

namespace RPGContent
{
    [ContentTypeWriter]
    //XNB writer for TileMapContent Objects.
    public class TileMapWriter : ContentTypeWriter<TileMapContent>
    {
        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "RPGEngine.TileMapReader, RPGEngine";
        }

        protected override void Write(ContentWriter output, TileMapContent value)
        {
            //Write in the Map width, height, tile width, and tile height into the XNB file.
            output.Write(value.MapWidth);
            output.Write(value.MapHeight);
            output.Write(value.TileWidth);
            output.Write(value.TileHeight);
            //Get the number of tile sets and write it to the .xnb
            int tileSetCount = value.TileSets.Count;
            output.Write(tileSetCount);
            //For each tile Set write the tileSetContent object into the .xnb file.
            foreach(TileSetContent tsc in value.TileSets)
            {
                output.WriteObject(tsc);
            }
            //get the number of layers in the tileMap content and write it to the .xnb
            int layersCount = value.Layers.Count;
            output.Write(layersCount);
            //For each Tile Layer write the TileLayerContent object to the .xnb.
            foreach (TileLayerContent tlc in value.Layers)
            {
                output.WriteObject(tlc);
            }
        }
    }

    [ContentTypeWriter]
    //XNB writer for TileSetContent Objects.
    public class TileSetWriter : ContentTypeWriter<TileSetContent>
    {
        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "RPGEngine.TileSetReader, RPGEngine";
        }

        protected override void Write(ContentWriter output, TileSetContent value)
        {
            //Write all the attributes of the TileSetContent to the .xnb.
            output.Write(value.Name);
            output.Write(value.Source);
            output.Write(value.TileWidth);
            output.Write(value.TileHeight);
            output.Write(value.ImageWidth);
            output.Write(value.ImageHeight);
            output.Write(value.FirstGid);
        }
    }

    [ContentTypeWriter]
    //XNB writer for TileLayerContent Objects.
    public class TileLayerWriter : ContentTypeWriter<TileLayerContent>
    {
        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "RPGEngine.TileLayerReader, RPGEngine";
        }

        protected override void Write(ContentWriter output, TileLayerContent value)
        {
            //Write the width height and name of the TileLayerContent .xnb
            output.Write(value.Width);
            output.Write(value.Height);
            output.Write(value.Name);
            //write the Gid info to the .xnb file
            for (int i = 0; i < value.GidInformation.Length; i++)
                output.Write(value.GidInformation[i]);
        }
    }
}
