﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
namespace RPGContent
{
    //Class to represent the contents of a Tilemap
    public class TileMapContent
    {
        public int MapWidth;
        public int MapHeight;
        public int TileWidth;
        public int TileHeight;
        public Collection<TileSetContent> TileSets = new Collection<TileSetContent>();
        public Collection<TileLayerContent> Layers = new Collection<TileLayerContent>();

    }

    //Class to represent the contents of a TileSet
    public class TileSetContent
    {
        public int FirstGid;
        public int TileHeight;
        public int TileWidth;
        public int ImageWidth;
        public int ImageHeight;
        public string Name;
        public string Source;
       
        
    }

    //Class to represent the contents of a Tile Layer
    public class TileLayerContent
    {
        public int[] GidInformation;
        public int Width;
        public int Height;
        public string Name;
    }
}
