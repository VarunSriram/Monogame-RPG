﻿using System;
using Microsoft.Xna.Framework.Content.Pipeline;
using System.Xml;
// TODO: replace these with the processor input and output types.


namespace RPGContent
{
    /// <summary>
    /// This class will be instantiated by the XNA Framework Content Pipeline
    /// to apply custom processing to content data, converting an object of
    /// type TInput to TOutput. The input and output types may be the same if
    /// the processor wishes to alter data without changing its type.
    ///
    /// This should be part of a Content Pipeline Extension Library project.
    ///
    /// TODO: change the ContentProcessor attribute to specify the correct
    /// display name for this processor.
    /// </summary>
    [ContentProcessor(DisplayName = "TMX Script Processor")]
    public class TMXProcessor : ContentProcessor<XmlDocument, TileMapContent>
    {
        public override TileMapContent Process(XmlDocument input, ContentProcessorContext context)
        {
            //Ininitilize instance of the tile map content.
            TileMapContent tileMapContent = new TileMapContent();
            //Get the Map Node of the .tmx file
            XmlNode mapNode = input.GetElementsByTagName("map")[0];
            //Get the width height tile width tile height attributes from the map node. 
            int width = int.Parse(mapNode.Attributes["width"].Value);
            int height = int.Parse(mapNode.Attributes["height"].Value);
            int tileWidth = int.Parse(mapNode.Attributes["tilewidth"].Value);
            int tileHeight = int.Parse(mapNode.Attributes["tileheight"].Value);

            //Set the parameters of the tile map content...
            tileMapContent.MapWidth = width;
            tileMapContent.MapHeight = height;
            tileMapContent.TileWidth = tileWidth;
            tileMapContent.TileHeight = tileHeight;

            //Find all tileset nodes from the .tmx file.
            XmlNodeList tileSetNodes = input.GetElementsByTagName("tileset");
            //for each tile set node...
            foreach(XmlNode tilesetNode in tileSetNodes)
            {
                //Create instance of tileSetContent...
                TileSetContent tsc = new TileSetContent();
                //Get the firstGid and name attribute of the tile set node.
                int firstgid = int.Parse(tilesetNode.Attributes["firstgid"].Value);
                string name = tilesetNode.Attributes["name"].Value;
                //Get the image source node...
                XmlNode imageSourceNode = tilesetNode.FirstChild;
                //Get the image source, width and height attributes from the node.
                string source = imageSourceNode.Attributes["source"].Value;
                int imageWidth = int.Parse(imageSourceNode.Attributes["width"].Value);
                int imageHeight = int.Parse(imageSourceNode.Attributes["height"].Value);

                //Construct the TilesetContnt.
                tsc.FirstGid = firstgid;
                tsc.TileHeight = tileHeight;
                tsc.TileWidth = tileWidth;
                tsc.ImageWidth = imageWidth;
                tsc.ImageHeight = imageHeight;
                tsc.Name = name;
                tsc.Source = source;

                //Add the tilesetcontent to the tileMapContent.
                tileMapContent.TileSets.Add(tsc);

            }
            //Get the layer nodes in the .tmx file
            XmlNodeList tileLayerNodes = input.GetElementsByTagName("layer");
            //For each layer node...
            foreach(XmlNode tileLayerNode in tileLayerNodes)
            {
                //Create tileLayerContent instance.
                TileLayerContent tlc = new TileLayerContent();
                //get the name attribute of the layer
                string layerName = tileLayerNode.Attributes["name"].Value;
                //find the data node of the layer.
                XmlNode dataNode = tileLayerNode.FirstChild;
                //Get the tile Nodes from the data node.
                XmlNodeList tileNodes = dataNode.ChildNodes;
                //For each tile node store the gid attribute as an integer array...
                int[] gidData = new int[tileNodes.Count];
                int i = 0;
                foreach(XmlNode tileNode in tileNodes)
                {
                    gidData[i] = int.Parse(tileNode.Attributes["gid"].Value);
                    i++;
                }

                //Construct the tilelayercontent.
                tlc.GidInformation = gidData;
                tlc.Width = width;
                tlc.Height = height;
                tlc.Name = layerName;

                tileMapContent.Layers.Add(tlc);
                
            }




            return tileMapContent;
        }
    }
}