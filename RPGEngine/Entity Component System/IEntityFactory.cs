﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEngine
{
    //Factory Abstrct class used for composing Entities.
    //Create a Factory for each entity needed and compose them by implementing the MakeEntity() class.
    public interface IEntityFactory
    {
         Entity MakeEntity();
    }
}
