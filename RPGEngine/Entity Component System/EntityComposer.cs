﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEngine
{
    public class EntityComposer
    {
        private Dictionary<string, IEntityFactory> factoryManifest;
         
        public EntityComposer()
        {
            factoryManifest = new Dictionary<string, IEntityFactory>();
        }

        public void RegisterEntityFactory(string entityName, IEntityFactory factory)
        {
            this.factoryManifest.Add(entityName, factory);
        } 

        public Entity ComposeEntity(string factoryName)
        {
            if (!factoryManifest.ContainsKey(factoryName))
            {
                throw new Exception("The Key " + factoryName + " Does not exist in the FactoryManifest!");
            }
            return this.factoryManifest[factoryName].MakeEntity();
        }


    }
}
