﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEngine
{   
    //Class to inherit from for components that need a container for values.
    public class EntityComponent : IEntityComponent
    {
        //Entity the component belongs to.
        private readonly Entity entity;
        //Name of the component.
        private string name;
        //Accessor of the Name.
        public string Name
        {
            get { return name;}
            set { name = value;}
        }
        //Accessor for the Entity.
        public Entity Entity
        {
            get { return entity; }
        }
        //Constructor requires a Entity to attach the component to.
        public EntityComponent(Entity aParent)
        {
            this.entity = aParent;
        }
        //Virtual method to initialize the component.
        public virtual void Initialize() { }
        //Virtual method to start the component.
        public virtual void Start() { }

    }
}
