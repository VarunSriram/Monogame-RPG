﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEngine
{
    public interface IEntityComponent
    {
        string Name { get; set; }

        //Initializes the Component.
        void Initialize();
        //called after the initization... 
        void Start();
    }
}
