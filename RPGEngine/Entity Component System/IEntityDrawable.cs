﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RPGEngine
{
    public interface IEntityDrawable
    {
        //Boolean to determine is the component is visible to screen.
        bool Visible { get; }

        //Int to rank what order the component should be drawn.
        int DrawOrder { get; }

        //Draw method to draw component to screen.
        void Draw(SpriteBatch spriteBath);
                     
        // Invoked when the Visible property changes.
        event EventHandler<EventArgs> VisibleChanged;

        // Invoked when the DrawOrder property changes. 
        event EventHandler<EventArgs> DrawOrderChanged;
    }
}
