﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEngine
{
   public interface IEntityUpdatable
   {
        //boolean to tell if the component should be updated or not.
        bool IsEnabled { get; }

        //Int which ranks the component to tell what order to update the component.
        int UpdateOrderNo { get; }

        //method to update the component.
        void Update(GameTime gameTime);

        //Invoked when the Enabled property changes.
        event EventHandler<EventArgs> EnabledChanged;

        //Invoked when the UpdateOrder property changes.      
        event EventHandler<EventArgs> UpdateOrderChanged;
    }
}
