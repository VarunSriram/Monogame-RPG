﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace RPGEngine
{
    //Class to present a Drawable Component. Inherit from this class to for components that draw to screen.
    public class DrawableEntityComponent : UpdatableEntityComponent, IEntityDrawable
    {
        //boolean to determine if the component is visible or not.
        private bool visible = true;
        //int to rank its draw order.
        private int drawOrder = 0;

        //Accessor for the draw order.
        public int DrawOrder
        {
            get
            {
                return drawOrder;
            }

            set
            {
                //If the value is different than what is stored...
                if(drawOrder != value)
                {
                    drawOrder = value;
                    //
                    if(DrawOrderChanged != null)
                    {
                        DrawOrderChanged(this, EventArgs.Empty);
                    }
                }
            }
        }
        //Accessor for the visibility.
        public bool Visible
        {
            get
            {
                return visible;
            }

            set
            {
                if(visible != value)
                {
                    visible = value;
                    //if the event handler is not null
                    if(VisibleChanged != null)
                    {
                        //fire off the event handler
                        VisibleChanged(this, EventArgs.Empty);
                    }
                }
            }
        }
        //Event handlers for when DrawOrder changes, and visibility changes.
        public event EventHandler<EventArgs> DrawOrderChanged;
        public event EventHandler<EventArgs> VisibleChanged;

       //Constructor for the DrawableEntityComponent.
       public DrawableEntityComponent(Entity parent) : base(parent)
        {
            VisibleChanged += OnVisibleChanged;
            DrawOrderChanged += OnDrawOrderChanged;
        }
        //Override of the initialize method.
        public override void Initialize()
        {
            //Load the content needed to draw.
            LoadContent();
            
        }

        //Overide the start method
        public override void Start()
        {
           
        }
        //Virtual methods to load and unload content.
        protected virtual void LoadContent() { }
        public virtual void UnloadContent() { }

        //overide of the update method.
        public override void Update(GameTime gameTime) { }
                        
        //Virtual method used to Draw
        public virtual void Draw(SpriteBatch spriteBatch){ }

        //Virtual method used to do an action when the visibility changes.
        protected virtual void OnVisibleChanged(object sender, EventArgs e) { }
        //Virtual method used to do an action when the draw order changes.
        protected virtual void OnDrawOrderChanged(object sender, EventArgs e) { }

    }
}
