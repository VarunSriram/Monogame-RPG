﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;


namespace RPGEngine
{
    //Class to represent an Entity in a game. An Entity is a Gameobject.
    public class Entity : GameObject
    {
        //Listing of all components in the entity used for lookups if needed.
        public Dictionary<string, IEntityComponent> Components = new Dictionary<string, IEntityComponent>();

        //Lists used for storing components, updatable components, and drawable components.
        private List<IEntityComponent> components = new List<IEntityComponent>();
        private List<IEntityUpdatable> updatableComponents = new List<IEntityUpdatable>();
        private List<IEntityDrawable> drawableComponents = new List<IEntityDrawable>();

        //Lists Updating and drawing are performed on.
        private List<IEntityComponent> tempComponents = new List<IEntityComponent>();
        private List<IEntityUpdatable> tempUpdatableComponents = new List<IEntityUpdatable>();
        private List<IEntityDrawable> tempDrawableComponents = new List<IEntityDrawable>();

        //Boolean to check if the entity has been initialized.
        private bool isInitialized = false;

        //Accessor for the number of components added to the entity. 
        public int ComponentCount
        {
            get { return components.Count; }
        }

        //Constuctor for an Entity
        public Entity(string name) : base(name)
        {
            Position = Vector2.Zero;
            Rotation = 0f;
            components.Clear();
            updatableComponents.Clear();
            drawableComponents.Clear();
        }

        //Initialize method to initialize the components of the of entity.
        public void Initialize()
        {
            //If the entity is already initialize return.
            if (isInitialized)
            {
                return;
            }
            //Refresh the tempComponents so it is up to date.
            tempComponents.Clear();
            tempComponents.AddRange(components);

            //Initialize each component in the temp components list
            for(int i = 0; i < tempComponents.Count; i++)
            {
                tempComponents[i].Initialize();
            }

            //Start each component in the temp components list
            for (int i = 0; i < tempComponents.Count; i++)
            {
                tempComponents[i].Start();
            }
            //Trip the initialize flag
            isInitialized = true;
        }


        //Method to update the updatable components.
        protected override void Update(GameTime gameTime)
        {
            //refresh the temp updatable components list
            tempUpdatableComponents.Clear();
            tempUpdatableComponents.AddRange(updatableComponents);
            //Update each updateable component if they are enabled.
            for (int i = 0; i < tempUpdatableComponents.Count; i++)
            {
                if (tempUpdatableComponents[i].IsEnabled)
                {
                    tempUpdatableComponents[i].Update(gameTime);
                }
            }

        }

        protected override void  Draw(SpriteBatch spriteBatch)
        {
            //refresh the temp drawable components list
            tempDrawableComponents.Clear();
            tempDrawableComponents.AddRange(drawableComponents);
            //Draw each drawable component if they are Visible.
            for (int i = 0; i < tempDrawableComponents.Count; i++)
            {
                if (tempDrawableComponents[i].Visible)
                {
                    tempDrawableComponents[i].Draw(spriteBatch);
                }
            }
        }

        //Method to add a component to the entity.
        public void AddComponent(EntityComponent component)
        {
            //If the Component is null throw an exception
            if (component == null)
            {
                throw new ArgumentNullException("Cannot add Component as it is Null");
            }
            //Remove this is needed Not sure if its nessessary yet.
            if (components.Contains(component))
            {
                return;
            }
            //Add the Component to the master list.
            components.Add(component);
            //Add the component to the look up dictionary.
            Components.Add(component.Name, component);

            IEntityUpdatable compUpdatable = component as IEntityUpdatable;
            IEntityDrawable compDrawable = component as IEntityDrawable;

            //If the component added is updatable add it to the updatable list and sort the list.
            if(compUpdatable != null)
            {
                updatableComponents.Add(compUpdatable);
                compUpdatable.UpdateOrderChanged += OnComponentUpdateOrderChanged;
                OnComponentUpdateOrderChanged(this, EventArgs.Empty);
            }
            //If the component added is Drawable add it to the drawable list and sort the list.
            if (compDrawable != null)
            {
                drawableComponents.Add(compDrawable);
                compDrawable.DrawOrderChanged += OnComponentDrawOrderChanged;
                OnComponentDrawOrderChanged(this, EventArgs.Empty);
            }

            //if the entity is already initialized call the components initialize and Start
            if (isInitialized)
            {
                component.Initialize();
                component.Start();
            }
           
        }

        //Method to remove a component from an entity.
        public bool RemoveComponent(IEntityComponent component)
        {
            //If the Component is null throw an exception
            if (component == null)
            {
                throw new ArgumentNullException("Cannot add Component as it is Null");
            }

            if (components.Remove(component))
            {
                IEntityUpdatable compUpdatable = component as IEntityUpdatable;
                IEntityDrawable compDrawable = component as IEntityDrawable;

                //If the component removed is updatable remove it to the updatable list and sort the list.
                if (compUpdatable != null)
                {
                    updatableComponents.Add(compUpdatable);
                    compUpdatable.UpdateOrderChanged -= OnComponentUpdateOrderChanged;
                  
                }
                //If the component removed is Drawable remove it to the drawable list and sort the list.
                if (compDrawable != null)
                {
                    drawableComponents.Add(compDrawable);
                    compDrawable.DrawOrderChanged -= OnComponentDrawOrderChanged;
                   
                }

                return true;
            }

            return false;
        }

       


        //Method to check if Entity has a component.
        public bool HasComponent(IEntityComponent component)
        {
            if (this.Components.ContainsKey(component.Name))
            {
                return true;
            }

            return false;
        }

        //Method to get a component given a component name.
        public UpdatableEntityComponent GetComponent<T>(string componentName)
        {
            if (this.Components.ContainsKey(componentName))
            {
                return Components[componentName] as UpdatableEntityComponent;
            }
            else
            {
                throw new ArgumentOutOfRangeException(componentName + " is not in the Entity's Colleciton of Component");
            }
        }

        //Method to get a component based on type.
        public T GetComponent<T>() where T : UpdatableEntityComponent
        {
            for(int i = 0; i < components.Count; i++)
            {
                if(components[i].GetType() == typeof(T))
                {
                    return components[i] as T;
                }
            }
            return null;
        }
        
        //change to override...
        public virtual void Destroy()
        {
            for(int i = 0; i < drawableComponents.Count; i++)
            {
                DrawableEntityComponent drawable = drawableComponents[i] as DrawableEntityComponent;
                drawable.UnloadContent();
            }

            RemoveAllComponents();
            //base.Destroy();
        }

        //Helper Methods----------------------------------------------

        void OnComponentUpdateOrderChanged(object sender, EventArgs e)
        {
            updatableComponents.Sort(UpdateableSort);
        }

        void OnComponentDrawOrderChanged(object sender, EventArgs e)
        {
            drawableComponents.Sort(DrawableSort);
        }

        private static int UpdateableSort(IEntityUpdatable aEntityA, IEntityUpdatable aEntityB)
        {
            return aEntityA.UpdateOrderNo.CompareTo(aEntityB.UpdateOrderNo);
        }

        private static int DrawableSort(IEntityDrawable aEntityA, IEntityDrawable aEntityB)
        {
            return aEntityA.DrawOrder.CompareTo(aEntityB.DrawOrder);
        }

        private void RemoveAllComponents()
        {
            for(int i = components.Count -1; i >= 0; i--)
            {
                RemoveComponent(components[i]);
            }

            components.Clear();
        }

    }
}
