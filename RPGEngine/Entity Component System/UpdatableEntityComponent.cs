﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEngine
{
    //Class representing a component that updates each frame.
    public class UpdatableEntityComponent : EntityComponent, IEntityUpdatable
    {
        //Boolean for enabling the component.
        private bool enabled = true;
        //int to rank the update order of the component
        private int updateOrder = 0;
        //Entity the component is attached to.
        private readonly Entity entity;
        //Event handlers for when the enable flag or the update order changes.
        public event EventHandler<EventArgs> EnabledChanged;
        public event EventHandler<EventArgs> UpdateOrderChanged;
       
        //Accessor for the Update order number
        public int UpdateOrderNo
        {
            get
            {
                return updateOrder;
            }
            set
            {
                //Set the update order value and fire off the event
                if (updateOrder != value)
                {
                    updateOrder = value;
                    UpdateOrderChanged(this, EventArgs.Empty);
                }
            }
        }

        //Accessor for the Enable flag.
        public bool IsEnabled
        {
            get
            {
                return enabled;
            }

            set
            {
                //Set the value and fire off the event.
                if (enabled != value)
                {
                    enabled = value;
                    if (EnabledChanged != null)
                    {
                        EnabledChanged(this, EventArgs.Empty);
                    }
                }
            }
        }

        //Constructor for the component
        public UpdatableEntityComponent(Entity aParent) 
            : base(aParent)
        {
            //exposed events as virtual methods
            EnabledChanged += OnEnabledChanged;
            UpdateOrderChanged += OnUpdateOrderChanged;
        }

        //Virtual method for updating the component each frame.
        public virtual void Update(GameTime gameTime) { }

        //Virtual methods to define what to when the enable/update order change event occurs.
        protected virtual void OnEnabledChanged(object sender, EventArgs e) { }

        protected virtual void OnUpdateOrderChanged(object sender, EventArgs e) { }
    }
}
