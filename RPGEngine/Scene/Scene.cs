﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEngine
{

    //Class to represent a generic Game Scene for GameObjects/Entities to interact in.
    //Inherit from this class for each scene in your game.
    public abstract class Scene
    {
        bool isInitialized = false;
        //Dictionary of all entities in the scene where the key is the name of the entity.
        private Dictionary<string, Entity> entitiesInScene;
        //name of the scene.
        private string name;
        //Entity composer to compose entites in the scene.
        protected EntityComposer entityComposer;

        //Accessor to get the name.
        public string Name
        {
            get { return name; }
        }


        //Acessor to the First Camera Component found in the scene 
        public Camera MainCamera
        {
            get
            {
                //Search for an Entity called camera.
                if (entitiesInScene.ContainsKey("MainCamera"))
                {
                    //Double check the Entity has a Camera component inside.
                    Camera camera = this.Find("MainCamera").GetComponent<Camera>() as Camera;
                    if (camera != null)
                    {
                        return camera;
                    }
                }
                //If there is no camera component in the scene throw this exception.
                throw new Exception("No Entity With a Camera Component Exists in the " + name +" Scene!");
               

            }
        }


        public Scene(string name)
        {
            this.name = name;
            this.entityComposer = new EntityComposer();
            entitiesInScene = new Dictionary<string, Entity>();
            this.SetupEntityComposer();
            this.SetupScene();
        }

        //Abstract methods to for instructions during entering/Exiting a scene.
        protected abstract void OnSceneEnter();

        protected abstract void OnSceneExit();

        //public methods for entering and exiting a scene.
        public void Enter()
        {
            this.OnSceneEnter();
        }

        public void Exit()
        {
            this.OnSceneExit();
        }

        //abstract method used for registering entity factories 
        protected abstract void SetupEntityComposer();
        //abstract method for the initial setup entities in a scene
        protected abstract void SetupScene();

        //Method to add an Entity to the scene.
        public void RegisterEntity(string name, Entity e)
        {
            if (isInitialized)
            {
                e.Initialize();
            }
            if (!entitiesInScene.ContainsKey(name))
            {
                e.Scene = this;
                entitiesInScene.Add(name, e);
            }
            else
            {
                throw new Exception("Entity Exists in Scene With The Same Name Use a Different Name");
            }
        }

        //Method to remove an entity from a scene.
        public void DeRegisterEntity(string name)
        {
            Entity e = entitiesInScene[name];
            entitiesInScene.Remove(name);
            e.Destroy();
        }

        public virtual void Initialize()
        {
            if (isInitialized)
            {
                return;
            }

            foreach (KeyValuePair<string, Entity> entry in entitiesInScene)
            {
                entry.Value.Initialize();
                isInitialized = true;
            }

        }

        public virtual void Update(GameTime gameTime)
        {
            foreach (KeyValuePair<string, Entity> entry in entitiesInScene)
            {
                entry.Value.PerformUpdate(gameTime);
                isInitialized = true;
            }
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            foreach (KeyValuePair<string, Entity> entry in entitiesInScene)
            {
                entry.Value.PerformDraw(spriteBatch);
            }
        }

        public Entity Find(string entityName)
        {
            if (entitiesInScene.ContainsKey(entityName))
            {
                return entitiesInScene[entityName];
            }

            return null;
        }

    }
}
