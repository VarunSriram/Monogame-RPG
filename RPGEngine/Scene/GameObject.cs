﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEngine
{
    //An Abstract class to represent all Game objects in the engine
    public abstract class GameObject
    {
        //boolean to tell if the game object is active.
        private bool isActive = true;
        
        //Transform Information of the game object.
        private Vector2 position = Vector2.Zero;
        private float rotation = 0f;

        //Scene the Game object belongs to.
        private Scene scene;

        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        //Accessor of the Scene.
        public Scene Scene
        {
            get { return scene; }
            set { this.scene = value; }
        }

        //Accessor of the Gameobject's position.
        public Vector2 Position
        {
            get { return position; }
            set { this.position = value; }
        }

        //Accessor of the Game objects rotation.
        public float Rotation
        {
            get { return rotation; }
            set { this.rotation = value; }
        }


        public GameObject(string name)
        {
            this.name = name;
        }

        //Method to perform a Drawing action
        public void PerformDraw(SpriteBatch spriteBatch)
        {
            if (this.isActive)
            {
                this.Draw(spriteBatch);
            }
        }

        //Method to Perform an Update action
        public void PerformUpdate(GameTime gameTime)
        {
            if (this.isActive)
            {
                this.Update(gameTime);
            }
        }

        //Abstract protected methods for implementing the Updates and drawing.
        protected abstract void Update(GameTime gameTime);
        protected abstract void Draw(SpriteBatch spriteBatch);

    }
}
