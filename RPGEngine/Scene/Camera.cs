﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEngine
{
    public class Camera : UpdatableEntityComponent
    {
        private float zoom = 3f;
        public float ZoomLevel
        {
            get { return zoom; }
            set { zoom = value; }
        }

        public Camera(Entity aParent) : base(aParent)
        {
            
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public Matrix TransformMatrix
        {
            get
            {
                return Matrix.CreateTranslation(new Vector3(-Entity.Position, 0f)) * Matrix.CreateScale(zoom,zoom,1);
            }
        }



    }
}
