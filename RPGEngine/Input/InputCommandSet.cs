﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;

namespace RPGEngine
{
    //Class to represent a set of input controls for a particular situation in a game.
    public class InputCommandSet
    {
        //Dictionaries to store input commands where string (command name) is the key and integer(Input Enum) as the value.
        private Dictionary<string, int> keyControls;
        private Dictionary<string, int> gamePadControls;
        private Dictionary<string, int> mouseControls;

        //Constructor for the command set.
        public InputCommandSet()
        {
            keyControls = new Dictionary<string, int>();
            gamePadControls = new Dictionary<string, int>();
            mouseControls = new Dictionary<string, int>();
        }

        //Method to map default controls to the keyboard, gamepad, and mouse.
        public void RegisterDefaultControls(string commandName,int keyboardKey, int gamepadButton, int mouseKey)
        {
            keyControls.Add(commandName, keyboardKey);
            gamePadControls.Add(commandName, gamepadButton);
            mouseControls.Add(commandName, mouseKey);
        }
        //method to check if a command assigned
        public bool IsCommandUnAssigned(string commandName)
        {
            
            bool keyboardExists = keyControls[commandName] < 0;
            bool gamePadExists = gamePadControls[commandName] < 0;
            bool mouseExists = mouseControls[commandName] < 0;

            //return that a mouse or keyboard, and gamepad key binding exists.
            return (keyboardExists || mouseExists) && gamePadExists;

        }


        //Method to set a keybord command...
        public void SetKeyBoardInput(string command,Keys key)
        {
            //See if a command in the keyboard commands already uses the passed in key
            string existingCommand = FindKeyboardCommand(key);
            //If a command does exist using the passed in key.
            if(existingCommand != null)
            {
                //Set the existing command to unassigned.
                keyControls[existingCommand] = -1;          
            }

            //Set the command to the key enum passed in.
            keyControls[command] = (int)key;
        }

        //Method to set a gamepad command...
        public void SetGamePadInput(string command, Buttons key)
        {
            //See if a command in the Game Pad commands already uses the passed in key
            string existingCommand = FindGamepadCommand(key);
            //If a command does exist using the passed in key.
            if (existingCommand != null)
            {
                //Set the existing command to unassigned.
                gamePadControls[existingCommand] = -1;
            }
            //Set the command to the key enum passed in.
            gamePadControls[command] = (int)key;
        }
        //Method to set a Mouse command...
        public void SetMouseInput(string command, MouseKeys key)
        {
            //See if a command in the Mouse commands already uses the passed in key
            string existingCommand = FindMouseCommand(key);
            if (existingCommand != null)
            {
                //Set the existing command to unassigned.
                mouseControls[existingCommand] = -1;
            }
            //Set the command to the key enum passed in.
            mouseControls[command] = (int)key;
        }

        //Method to find a command given a keyboard key.
        private string FindKeyboardCommand(Keys keyboardKey)
        {
            //for each key value pair....
            foreach (KeyValuePair<string, int> control in keyControls)
            {
                //If the value is equal to the keyboard key passed in return the command name.
                if (control.Value == (int)keyboardKey)
                {
                    return control.Key;
                }
            }

            return null;
        }
        //Method to find a command given a Gamepad button.
        private string FindGamepadCommand(Buttons button)
        {
            //for each key value pair....
            foreach (KeyValuePair<string, int> control in gamePadControls)
            {
                //If the value is equal to the Gamepad button passed in return the command name.
                if (control.Value == (int)button)
                {
                    return control.Key;
                }
            }

            return null;
        }
        //Method to find a command given a Mouse key.
        private string FindMouseCommand(MouseKeys mouseKey)
        {
            //for each key value pair...
            foreach (KeyValuePair<string, int> control in mouseControls)
            {
                //If the value is equal to the Mouse key passed in return the command name.
                if (control.Value == (int)mouseKey)
                {
                    return control.Key;
                }
            }
            //else return null.
            return null;
        }

        // Tell if an input command was pressed
        public bool IsCommandPressed(string command)
        {
            if (keyControls.ContainsKey(command))
            {
                throw new Exception("Command " + command + " Does not exist!");
            }
            //Using the input helper to check if the key and gamepad button is pressed.
            bool keyboardHeld = InputHelper.IsKeyPressed((Keys)keyControls[command]);
            bool gamepadHeld = InputHelper.IsGamePadButtonPressed((Buttons)gamePadControls[command]);
            //store the mouse button assigned to the mouse command.
            bool mouseKeyPressed = InputHelper.IsMouseButtonClicked((MouseKeys)mouseControls[command]);
          
          
            //If either the keyboard ,gamepad, or mouse key is pressed return true.
            return keyboardHeld || gamepadHeld || mouseKeyPressed;

        }
        //Check if an input command is held down.
        public bool IsCommandDown(string command)
        {
            if (keyControls.ContainsKey(command))
            {
                throw new Exception("Command "+command+" Does not exist!");
            }

            //Using the input helper to check if the key and gamepad button is Held down.
            bool keyboardHeld = InputHelper.IsKeyDown((Keys)keyControls[command]);
            bool gamepadHeld = InputHelper.IsGamePadButtonDown((Buttons)gamePadControls[command]);
            //bool to check if the mouse key of the command is pressed
            bool mouseKeyHeld = InputHelper.IsMouseButtonDown((MouseKeys)mouseControls[command]);
           
            //If either the keyboard ,gamepad, or mouse key is held down return true.
            return keyboardHeld || gamepadHeld || mouseKeyHeld;


        }



    }
}
