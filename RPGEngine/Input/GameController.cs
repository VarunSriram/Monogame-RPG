﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace RPGEngine
{
    //Class used for defining what to do when user input is provided.
    public abstract class GameController : UpdatableEntityComponent 
    {
        //Command set to determine what input the controller is responding to.
        protected InputCommandSet commands;
        //Accessor for the command set.
        public InputCommandSet CharacterCommands
        {
            get { return commands; }
            set { commands = value; }
        }


        public GameController(Entity e, InputCommandSet commands) : base(e)
        {
            this.commands = commands;
        }

        //Method to tell what to do when commands from the command set are set off.
        protected abstract void HandleInput(GameTime gameTime);

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            //Handle the input at every frame.
            HandleInput(gameTime);
        }

    }
}
