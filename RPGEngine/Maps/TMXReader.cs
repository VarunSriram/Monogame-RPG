﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace RPGEngine
{
    //Class to read XNB file format to build an instance of a TileMap.
    public class TileMapReader : ContentTypeReader<TileMap>
    {
        protected override TileMap Read(ContentReader input, TileMap existingInstance)
        {
            //Read the map width, height, tile width, and height from the XNB file.
            int mapWidth = input.ReadInt32();
            int mapHeight = input.ReadInt32();
            int tileWidth = input.ReadInt32();
            int tileHeight = input.ReadInt32();

            //Create an instance of the tile map.
            TileMap tileMap = new TileMap(mapWidth, mapHeight, tileWidth, tileHeight);
            //read in the number of tile sets.
            int tileSetCount = input.ReadInt32();
            for(int i = 0; i < tileSetCount; i++)
            {
                //Read in the tileSet and add it to the list of tileSets the tilemap has.
                TileSet ts = input.ReadObject<TileSet>();
                tileMap.AddTileSet(ts);
            }
            //read in the number of layers 
            int layerCount = input.ReadInt32();
            for(int i = 0; i < layerCount; i++)
            {
                //Read the Layer in and add it to the list layers the tilemap has.
                TileLayer tl = input.ReadObject<TileLayer>();
                tileMap.AddLayer(tl);
            }

            return tileMap;
        }
    }

    //class to read in TileSet objects from XNB.
    public class TileSetReader : ContentTypeReader<TileSet>
    {
        protected override TileSet Read(ContentReader input, TileSet existingInstance)
        {
            //read the Name, image source, tile width and height, image width and height, and the first Gid.
            string name = input.ReadString();
            string source = input.ReadString();
            int tileWidth = input.ReadInt32();
            int tileHeight = input.ReadInt32();
            int imageWidth = input.ReadInt32();
            int imageHeight = input.ReadInt32();
            int firstGid = input.ReadInt32();
            //Return new instance tile Set given the info read in from the file.
            TileSet tileSet = new TileSet(firstGid, name, tileWidth, tileHeight, source, imageWidth, imageHeight);
            return tileSet;
        }
    }

    //class to read TileLayer objects from XNB
    public class TileLayerReader : ContentTypeReader<TileLayer>
    {
        protected override TileLayer Read(ContentReader input, TileLayer existingInstance)
        {
            //Read in the width height and name of the layer.
            int width = input.ReadInt32();
            int height = input.ReadInt32();
            string name = input.ReadString();
            // initialize a new int array to store the gid info read in from XNB.
            int[] gidInfo = new int[width * height];
            for(int i = 0; i < (width*height); i++)
            {
                //read gid informaiton from XNB
                gidInfo[i] = input.ReadInt32();
            }

            //return new instance of Tile Layer given the info read in from XNB.
            TileLayer tileLayer = new TileLayer(name, width, height, gidInfo);

            return tileLayer;
        }
    }

    
}
