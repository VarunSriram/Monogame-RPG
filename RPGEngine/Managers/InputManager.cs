﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;

namespace RPGEngine
{
    //Singleton manager class for handing user input commands.
    public class InputManager
    {
        private static InputManager instance;
        //private dictionary to store command sets.
        private Dictionary<string, InputCommandSet> commandSets;

        private InputManager()
        {
            commandSets = new Dictionary<string, InputCommandSet>();
        }

        public static InputManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new InputManager();
                }
                return instance;
            }
        }
       
       //Method to retrieve a command set. 
       public InputCommandSet GetCommandSet(string commandSet)
       {
            return commandSets[commandSet];
       }
       //Method to register a command set.
       public void RegisterCommandSet(string commandSetName, InputCommandSet commandSet)
        {
            commandSets.Add(commandSetName, commandSet);
        }

    }
}

//Enum to enumerate mouse key names.
public enum MouseKeys
{
    LeftClick,
    RightClick,
    MiddleClick
}


