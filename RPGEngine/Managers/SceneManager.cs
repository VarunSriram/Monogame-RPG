﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEngine
{
    //Singleton manager class to manage scenes in a game.
    public class SceneManager
    {
        
        private static SceneManager instance;
        //The current scene active.
        private Scene currentScene;
        //Dictionary to store scenes using the name of the scene as a key.
        private Dictionary<string, Scene> scenesCatalog = new Dictionary<string, Scene>();

        //Accessor to get the current scene.
        public Scene CurrentScene
        {
            get { return currentScene; }
        }



        private SceneManager()
        {

        }


        public static SceneManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SceneManager();
                }
                return instance;
            }
        }

        //Method to register a scene to the dictionary
        public void RegisterScene(Scene scene)
        {
            //If there are no scenes stored set the current scene to the one passed in.
            if(scenesCatalog.Count == 0)
            {
                currentScene = scene;
            }

            //add the scene to the dictionary.
            this.scenesCatalog.Add(scene.Name, scene);

        }
        //Method to switch a scene.
        public void SwitchScene(string name)
        {
            //If the dictionary does not have a key with the name passed in...
            if (!scenesCatalog.ContainsKey(name))
            {
                //throw an exception stating no scene with that name exists.
                throw new Exception("Scene manager does not have a scene with that name!");
            }
            //If the current scene is not null call the Exit functions of the scene.
            if(this.currentScene != null)
            {
                currentScene.Exit();
            }
            //set the current scene to the one requested.
            this.currentScene = scenesCatalog[name];
            //Call the enter functions of the scene.
            this.currentScene.Enter();
        }



    }
}
