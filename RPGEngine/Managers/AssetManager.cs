﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEngine
{
    //Manager Singleton for loading assets from the Monogame Content Pipeline.
    public class AssetManager
    {
        private static AssetManager instance;
        //Instance of the content manager needed to load.
        private ContentManager contentManager;
        //Accessor for to set the Content Manager
        public ContentManager ContentManager
        {
            set { contentManager = value; }
        }

        private AssetManager()
        {

        }

        public static AssetManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AssetManager();
                }
                return instance;
            }
        }
        

        //Method to load texture.
        public Texture2D LoadTexture(string dir)
        {
            return contentManager.Load<Texture2D>(dir);
        }

        //Method to load a TileMap.
        public TileMap LoadTileMap(string dir)
        {
            return contentManager.Load<TileMap>(dir);
        }
    }
    
}
