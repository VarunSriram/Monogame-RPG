﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace RPGEngine
{
    //Static helper class for reading user input from devices.
    public static class InputHelper
    {
        // New and Old Keyboard States
        static KeyboardState newKeyState;
        static KeyboardState oldKeyState;

        //New and Old Game Pad states
        static GamePadState newGamePadState;
        static GamePadState oldGamePadState;

        //New and Old Mouse States
        static MouseState newMouseState;
        static MouseState oldMouseState;

        //Update method to update input states.
        public static void Update()
        {
            oldKeyState = newKeyState;
            newKeyState = Keyboard.GetState();

            oldGamePadState = newGamePadState;
            newGamePadState = GamePad.GetState(PlayerIndex.One);

            oldMouseState = newMouseState;
            newMouseState = Mouse.GetState();
        }

        //Method to check if a key was pressed
        public static bool IsKeyPressed(Keys key)
        {
            return (newKeyState.IsKeyDown(key) && oldKeyState.IsKeyUp(key));
        }

        //Method to check if a key is held down.
        public static bool IsKeyDown(Keys key)
        {
            return newKeyState.IsKeyDown(key);
        }

        //Method to check if a  gamepad button was pressed
        public static bool IsGamePadButtonPressed(Buttons button)
        {
            return (newGamePadState.IsButtonDown(button) && oldGamePadState.IsButtonUp(button));
        }

        //Method to check if a gamepad button is held down
        public static bool IsGamePadButtonDown(Buttons button)
        {
            return (newGamePadState.IsButtonDown(button));
        }

        //Check if the left mouse button is clicked.
        private static bool IsLeftMouseClicked()
        {
            return (newMouseState.LeftButton == ButtonState.Pressed && oldMouseState.LeftButton == ButtonState.Released);
        }
        //Check if the left mouse button is held down.
        private static bool IsLeftMouseDown()
        {
            return (newMouseState.LeftButton == ButtonState.Pressed);
        }

        //Check if the right mouse button is clicked.
        private static bool IsRightMouseClicked()
        {
            return (newMouseState.RightButton == ButtonState.Pressed && oldMouseState.RightButton == ButtonState.Released);
        }

        //Check if the right mouse button is held down.
        private static bool IsRightMouseDown()
        {
            return (newMouseState.RightButton == ButtonState.Pressed);
        }

        //Check if the middle mouse button is clicked.
        private static bool IsMiddleMouseClicked()
        {
            return (newMouseState.MiddleButton == ButtonState.Pressed && oldMouseState.MiddleButton == ButtonState.Released);
        }

        //Check if the middle mouse button is held down.
        private static bool IsMiddleMouseDown()
        {
            return (newMouseState.MiddleButton == ButtonState.Pressed);
        }

        //Check if a mouse button is clicked.
        public static bool IsMouseButtonClicked(MouseKeys mouseKey)
        {
            bool mouseKeyClicked;
            switch (mouseKey)
            {                
                //Is the left mouse button assigned
                case MouseKeys.LeftClick:
                    //check if the left mouse is clicked.
                    mouseKeyClicked = InputHelper.IsLeftMouseClicked();
                    break;
                //is the right mouse button  is clicked.
                case MouseKeys.RightClick:
                    //check if the right mouse is held down.
                    mouseKeyClicked = InputHelper.IsRightMouseClicked();
                    break;
                //is the middle mouse button  is clicked.
                case MouseKeys.MiddleClick:
                    //check if the middle mouse is held down.
                    mouseKeyClicked = InputHelper.IsMiddleMouseClicked();
                    break;
                //is the mouse button unassigned.
                default:
                    //set to false.
                    mouseKeyClicked = false;
                    break;
            }
            return mouseKeyClicked;
        }

        //Check if a mouse button is held down.
        public static bool IsMouseButtonDown(MouseKeys mouseKey)
        {
            bool mouseKeyDown;
            switch (mouseKey)
            {
                //Is the left mouse button assigned
                case MouseKeys.LeftClick:
                    //check if the left mouse is held down.
                    mouseKeyDown = InputHelper.IsLeftMouseDown();
                    break;
                //is the right mouse button assigned.
                case MouseKeys.RightClick:
                    //check if the right mouse is held down.
                    mouseKeyDown = InputHelper.IsRightMouseDown();
                    break;
                //is the middle mouse button assigned.
                case MouseKeys.MiddleClick:
                    //check if the middle mouse is held down.
                    mouseKeyDown = InputHelper.IsMiddleMouseDown();
                    break;
                //is the mouse button unassigned.
                default:
                    //set to false.
                    mouseKeyDown = false;
                    break;
            }
            return mouseKeyDown;
        }





        //Method to get read the  tilt for the left thumb stick.
        public static Vector2 GetLeftThumbStickAxis(bool normalize)
        {
            Vector2 motion = new Vector2(newGamePadState.ThumbSticks.Left.X, -newGamePadState.ThumbSticks.Left.Y);

            if (normalize)
            {

                motion.Normalize();
            }

            return motion;
        }

        //Method to read tge tilt for the right thumb stick.
        public static Vector2 GetRightThumbStickAxis(bool normalize)
        {
            Vector2 motion = new Vector2(newGamePadState.ThumbSticks.Right.X, -newGamePadState.ThumbSticks.Right.Y);

            if (normalize)
            {
                motion.Normalize();
            }

            return motion;
        }

        //Method to get the position of the mouse 
        public static Vector2 GetMousePosition()
        {
            return new Vector2(newMouseState.X, newMouseState.Y);
        }

    }
}
