﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace RPGEngine
{
    public class AnimatedSpriteRenderer : DrawableEntityComponent
    {
        private string spriteSheetDirectory;
        //Dictionary to store our animations based on string key. where the string is a label describing what animation.
        public Dictionary<string, FrameAnimator> Animations = new Dictionary<string, FrameAnimator>();
        //string telling what animation is being used
        string currentAnimation = null;
        //bool to toggle the animation.
        bool animating = true;
        //Texture of the sprite.
        Texture2D texture;

        //Accessor to tell if the animation is running.
        public bool IsAnimating
        {
            get { return animating; }
            set { animating = value; }
        }
        //Accessor for the current FrameAnimation running.
        public FrameAnimator CurrentAnimation
        {
            get
            {
                if (!string.IsNullOrEmpty(currentAnimation))
                    return Animations[currentAnimation];
                else
                    return null;
            }
        }

        //Accessor for the current animation's name string.
        public string CurrentAnimationName
        {
            get { return currentAnimation; }
            set
            {
                if (Animations.ContainsKey(value))
                    currentAnimation = value;
            }

        }


        public AnimatedSpriteRenderer(Entity e, string spriteSheetDir) : base(e)
        {
            spriteSheetDirectory = spriteSheetDir;
        }

        //Inherited initialize method.
        public override void Initialize()
        {
            //Base class's initialize method loads content by default.
            base.Initialize();
        }

        protected override void LoadContent()
        {
            this.texture = AssetManager.Instance.LoadTexture(this.spriteSheetDirectory);
        }

        //Override of the Draw method  to draw the tilemap to screen.
        public override void Draw(SpriteBatch spriteBatch)
        {
            FrameAnimator animation = CurrentAnimation;
            if (animation != null)
                spriteBatch.Draw(texture, Entity.Position, animation.CurrentRect, Color.White);

        }


        public override void Update(GameTime gameTime)
        {
            //if the animation is not animating disregard the update.
            if (!IsAnimating)
            {
                return;
            }
            FrameAnimator animation = CurrentAnimation;
            //Checking the animation is not null;
            if (animation == null)
            {
                //if an animation exists reset to first animation.
                if (Animations.Count > 0)
                {
                    string[] keys = new String[Animations.Count];
                    Animations.Keys.CopyTo(keys, 0);
                    currentAnimation = keys[0];
                    animation = CurrentAnimation;
                }
                else
                {
                    return;
                }
            }
            animation.Update(gameTime);
        }

    }
}
