﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEngine
{
    //A renderer Component for tile maps. Attach to an entity that you want to draw a tile map with.
    public class TileMapRenderer : DrawableEntityComponent
    {
        //Tile map data to refer to.
        private TileMap tileMap;
        //list of textures for the tile sheets.
        private Dictionary<string,Texture2D> tileSetSheets;
        //string for the directory of the TMX map.
        private string tileMapDir;

        //String for the folder directory of the .png tile sheets.
        private string folderDir;

        //boolean to allow for Camera Culling.
        private bool allowCulling = true;

        //Setter for the allow culling flag.
        public bool Culling
        {
            set { allowCulling = value; }
        }


        //Accessor for the Tile Map directory string. 
        public  string TileMapDirectory
        {
            get { return tileMapDir; }
            set { tileMapDir = value; }
        }

        public TileMap TileMap
        {
            get { return tileMap; }
        }

        //Constructor 
        public TileMapRenderer(Entity e,string tileMapDir, string folderDir) : base(e)
        {
            this.tileMapDir = tileMapDir;
            this.folderDir = folderDir;
            tileSetSheets = new Dictionary<string, Texture2D>();
        }

        //Inherited initialize method.
        public override void Initialize()
        {
            //Base class's initialize method loads content by default.
            base.Initialize();
        }
        //Override the load content menthod to load in the tilemap and the tile set textures.
        protected override void LoadContent()
        {
            tileMap = AssetManager.Instance.LoadTileMap(tileMapDir);
            foreach(TileSet ts in tileMap.TileSets)
            {
                tileSetSheets.Add(ts.Name,AssetManager.Instance.LoadTexture(folderDir+ts.Name));
            }
        }
        //Override of the Draw method  to draw the tilemap to screen.
        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);

            int tileWidth = tileMap.TileWidth;
            int tileHeight = tileMap.TileHeight;

            Camera renderCamera = this.Entity.Scene.MainCamera;

            int lowerBoundX;
            int upperBoundX;
            int lowerBoundY;
            int upperBoundY;
            //If culling flag is set then set the drawing boundaries based on the viewport of the scene's camera
            if (allowCulling)
            {
                Point min = ConvertPositionToCell(renderCamera.Entity.Position);
                Point max = ConvertPositionToCell(renderCamera.Entity.Position + new Vector2(
                    spriteBatch.GraphicsDevice.Viewport.Width + tileMap.TileWidth,
                    spriteBatch.GraphicsDevice.Viewport.Height + tileMap.TileHeight));
                if (min.X < 0)
                {
                    min.X = 0;
                }
                if (min.Y < 0)
                {
                    min.Y = 0;
                }

                if(max.X> tileMap.MapWidth)
                {
                    max.X = tileMap.MapWidth;
                }

                if(max.Y > tileMap.MapHeight)
                {
                    max.Y = tileMap.MapHeight;
                }

                lowerBoundX = min.X;
                upperBoundX = max.X;
                lowerBoundY = min.Y;
                upperBoundY = max.Y;
            }
            //If culling flag not set set the drawing a boundaries based on the tilemap dimensions.
            else
            {
                lowerBoundX = 0;
                upperBoundX = tileMap.MapWidth;
                lowerBoundY = 0;
                upperBoundY = tileMap.MapHeight;
            }






            //Begin the sprite batch.
            spriteBatch.Begin(SpriteSortMode.Texture, null, SamplerState.PointClamp, null, null, null, renderCamera.TransformMatrix);
            //Iterate through the tiles within the drawing boundaries
           
            foreach (TileLayer tl in tileMap.TileLayers)
            {
                if (tl.Visibility)
                {
                    //For each tile within the bounds of the camera's view...
                    for (int y = lowerBoundX; y < upperBoundX; y++)
                    {
                        for (int x = lowerBoundY; x < upperBoundY; x++)
                        {
                            //Grab the Gid from the tilemap given the position.
                            int gid = tl.getGid(y, x);
                            //if no tile exists continue on...
                            if (gid == 0)
                            {
                                continue;
                            }
                            //Find the tile set the Gid belongs to
                            TileSet tileset = tileMap.FindTileSet(gid);
                            //Get the tilesheet from the tile set.
                            Texture2D texture = this.tileSetSheets[tileset.Name];

                            gid -= tileset.FirstGid-1;
                            //Calculate the x,y pixel coordinates within the tile sheet the specific tile is located 
                            int sourceY = (int)(Math.Ceiling(((float)(gid) / (float)(tileset.TileAmountWidth)))) -1;
                            int sourceX = gid - (tileset.TileAmountWidth * sourceY) - 1;
                            //Vector2 drawPos = new Vector2(y * tileWidth, x * tileHeight);
                            //Create the draw rectangle which defines the tile's location and size on screen.
                            Rectangle drawRect = new Rectangle(new Point(y*tileWidth,x*tileHeight), new Point(tileWidth, tileHeight));
                            //Create the source rectangle which defines where on the tile sheet the tile is cropped.
                            Rectangle sourceRect = new Rectangle(sourceX * tileWidth, sourceY * tileHeight, tileWidth, tileHeight);
                            //Draw the tile to screen.
                            spriteBatch.Draw(texture, drawRect, sourceRect, Color.White);

                        }
                    }
                }
            }
            spriteBatch.End();


         }
       
        Point ConvertPositionToCell(Vector2 position)
        {
           
            return new Point((int)(position.X / (float)tileMap.TileWidth), (int)(position.Y / (float)tileMap.TileWidth));
        }
        
    }
}
